/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32f4xx.h"
#include "stm32f411e_discovery.h"

#define TIM_ARR         (uint16_t)1000
#define TIM_CCR1        (uint16_t)100

void PWM();
static void TIM4_Config(void);
static void SystemClock_Config(void);
static void Error_Handler(void);

TIM_HandleTypeDef htim4;
TIM_OC_InitTypeDef sConfigTim4;

int main(void)
{
  HAL_Init();
  SystemClock_Config();
  BSP_LED_Init(LED6);
  TIM4_Config();
  while(1)
  {
    PWM();
    HAL_TIM_PWM_MspInit(&htim4);
    BSP_LED_On(LED6);
  }
}

void PWM()
{
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
}

static void TIM4_Config(void)
{

  /* Time base configuration */
  htim4.Instance             = TIM4;
  htim4.Init.Period          = TIM_ARR;
  htim4.Init.Prescaler       = 100; 
  htim4.Init.ClockDivision   = 0; 
  htim4.Init.CounterMode     = TIM_COUNTERMODE_UP;
  HAL_TIM_PWM_Init(&htim4);

  /* TIM PWM1 Mode configuration: Channel-1 */
  sConfigTim4.OCMode = TIM_OCMODE_PWM1;
  sConfigTim4.OCIdleState = TIM_OCIDLESTATE_SET;
  sConfigTim4.Pulse = TIM_CCR1;
  HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_1);
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  if(htim_pwm->Instance==TIM4)
  {
    __TIM4_CLK_ENABLE();

    GPIO_InitStruct.Pin = GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
  }
}

static void SystemClock_Config(void)
{
 RCC_ClkInitTypeDef RCC_ClkInitStruct;
 RCC_OscInitTypeDef RCC_OscInitStruct;
 /* Enable Power Control clock */
 __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /* Enable HSI Oscillator and activate PLL with HSI as src */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
  Error_Handler();
  }
  RCC_ClkInitStruct.ClockType =
  (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
  RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) !=
 HAL_OK)
  {
  Error_Handler();
  }
}

static void Error_Handler(void)
{
  while(1)
  {

  }
}