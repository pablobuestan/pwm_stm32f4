Using STM32F411E Discovery card with the CUBE HAL Library and the development environment "System Workbench":

This example shows how to configure the TIM peripheral in PWM mode.
The most appropiate way to launch this example is: At the address: NAME_OF_PROJECT/inc and NAME_OF_PROJECT/src, replace the original files with those available in this repository. With these steps, the project can be compiled correctly.

at the file main.c:

At the beginning of the main program the HAL_Init() function is called to reset all the peripherals, initialize the Flash interface and the systick.
Then the SystemClock_Config() function is used to configure the system clock (SYSCLK) to run at 100 MHz.

The function static void TIM4_Config(void) configure TIM4, Channel 1 as PWM mode where:
  - htim4.Init.Period.- It is the period of the account. If the account reaches that number (TIM_ARR = 1000), the interruption is launched and the account is set with a value = 0.
  - htim4.Init.Prescaler.- It can be configured to reduce the clock frequency of the timer input. In this case, a value of 100 has been set to obtain a clock frequency of 1MHz.
With these two values selected a PWM at a frequency of 1KHz is obtained.
  - sConfigTim4.Pulse = TIM_CCR1.- The duty cycle is configured (TIM_CCR1 = 500). With this value the signal will be on and off for 50%, respectively.

The function void void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm) configure PD12 as output.